# jinja2-templates

A collection of Jinja2 templates available for use with Itential's [templateIDE](https://template.itential.io)

## Overview

Jinja2 Templates are stored with their YAML counterparts in the `templates` directory. The naming convention is derived from other standards that have been set forth. This standard is parseable and therefore easy to consume by outside machinary.

`<vendor>_<os>_<command>.j2`
`<vendor>_<os>_<command>.yml`

## Contributing

Contributions can be made via merge request.

## Supporting Technologies

[Jinja2](https://jinja.palletsprojects.com/en/2.10.x/)
[YAML](https://yaml.org/)
